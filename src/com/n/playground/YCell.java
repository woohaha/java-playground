package com.n.playground;

public class YCell implements Cell {

    private long grown = 0;
    private int MULTIPLY_TIME = 2;
    private int lifeTime;

    public YCell setGrown(int grown) {
        this.grown = grown;
        return this;
    }

    @Override
    public void killed(long getKilled) {
        grown = getKilled > grown ? 0 : grown - getKilled;
    }

    public void tick() {
        lifeTime++;
        if (lifeTime % MULTIPLY_TIME == 0) {
            generate();
        }
    }

    private void generate() {
        grown *= 2;
    }

    public long getGrown() {
        return grown;
    }
}
