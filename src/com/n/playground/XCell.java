package com.n.playground;

public class XCell implements Cell {
    private Cell food;
    private long grown = 0;
    private long newBorn;
    private int MULTIPLY_TIME = 3;
    private int lifeTime;

    public long getGrown() {
        return grown;
    }

    public XCell setFood(Cell food) {
        this.food = food;
        return this;
    }

    public XCell setNewBorn(int newBorn) {
        this.newBorn = newBorn;
        return this;
    }

    public void tick() {
        if (lifeTime == 0) {
            food.killed(newBorn);
        }
        lifeTime++;
        if (lifeTime % MULTIPLY_TIME == 0) {
            multiple();
        }
        eat();
    }

    private void multiple() {
        grown += newBorn;
        newBorn = grown * 2;
        food.killed(newBorn);
    }

    private void eat() {
        food.killed(grown);
    }

    @Override
    public void killed(long hunter) {
        System.out.println("Nothing can kill this cell");
    }

}
