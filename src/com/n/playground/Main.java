package com.n.playground;

public class Main {

    public static void main(String[] args) {

        XCell xCell = new XCell();
        YCell yCell = new YCell();

        yCell.setGrown(90);
        xCell.setNewBorn(10)
                .setFood(yCell);

        for (int minute = 0; minute < 60; minute++) {
            yCell.tick();
            xCell.tick();
            System.out.println("第" + minute + "分鐘");
            System.out.println("x的數量爲" + xCell.getGrown());
            System.out.println("y的數量爲" + yCell.getGrown());
        }

    }
}
